package com.ipass.escopocielo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import cielo.orders.domain.Credentials
import cielo.sdk.order.OrderManager
import cielo.sdk.order.ServiceBindListener
import kotlinx.android.synthetic.main.activity_kotlin.*
import cielo.sdk.order.payment.PaymentError
import cielo.orders.domain.Order
import cielo.printer.client.PrinterAttributes
import cielo.sdk.order.PrinterListener
import cielo.sdk.order.payment.PaymentCode
import cielo.sdk.order.payment.PaymentListener
import cielo.sdk.printer.PrinterManager
import org.jetbrains.annotations.NotNull


class KotlinActivity : AppCompatActivity() {
    var orderId: String? = null
    var orderManager: OrderManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlin)

        toolbar.title = "Tipo de Pagamento"
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val pedido = intent.getIntExtra("pedido", 0)

        tv_produto_descricao.text = if(pedido == 0)
            "Produto: Coca-cola\nPreço: RS 5,00\nQuantidade: 1"
        else
            "Produto: Cerveja\nPreço: RS 8,00\nQuantidade: 1"


        btn_pagamento_parcial.setOnClickListener{ pagamento(1) }
        btn_pagamento_valor.setOnClickListener { pagamento(2) }
        btn_pagamento_credito.setOnClickListener { pagamento(3) }
        btn_pagamento_debito.setOnClickListener { pagamento(4) }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item!!.itemId == android.R.id.home) finish()

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()

        //Configurar credenciais
        val credentials = Credentials(getString(R.string.client_id_cielo), getString(R.string.access_token_cielo))

        //Request login
        orderManager = OrderManager(credentials, this)

        //Callback do servidor cielo
        val serviceBindListener = object: ServiceBindListener{
            override fun onServiceBound() {
                Log.d("CIELO", "Foi conectado com o cielo")
                criarPedido()
            }

            override fun onServiceBoundError(throwable: Throwable) {
                Log.d("CIELO", "Ocorreu um erro ao tentar se conectar com o servico Order Manager")
            }

            override fun onServiceUnbound() {
                Log.d("CIELO", "O serviço foi desvinculado")
            }
        }
        orderManager!!.bind(this, serviceBindListener)
    }

    fun criarPedido(){

        //criar um pedido(order) e adicionar itens
        val order = orderManager!!.createDraftOrder("PEDIDO_01")

        //Identificação produto
        val nome = "Coca-cola"
        val cod_barras = "2891820317391823"

        //Preço unitário em CENTAVOS
        val precoCentavos: Long = 550
        val quantidade = 1

        //Unidade de medida
        val uniMedida = "Unidade"

        //adicionar item ao pedido
        order!!.addItem(cod_barras, nome, precoCentavos, quantidade, uniMedida)

        //Liberar pedido para pagamento
        orderManager!!.placeOrder(order)

        orderId = order.id
    }

    fun pagamento(tipoPgto: Int){

        //Callback do pagamento
        val paymentListener = object : PaymentListener {
            override fun onStart() {
                Log.d("CIELO", "O pagamento começou.")
            }

            override fun onPayment(@NotNull order: Order) {
                Log.d("CIELO", "Um pagamento foi realizado.")
               // impressao()
            }

            override fun onCancel() {
                Log.d("CIELO", "A operação foi cancelada.")
            }

            override fun onError(@NotNull paymentError: PaymentError) {
                Log.d("CIELO", "Houve um erro no pagamento.")
            }
        }

        if(tipoPgto == 1){
            orderManager!!.checkoutOrder(orderId!!, paymentListener)
        }
        else if(tipoPgto == 2){
            orderManager!!.checkoutOrder(orderId!!, 550, paymentListener)
        }
        else if(tipoPgto == 3){
            orderManager!!.checkoutOrder(orderId!!, 550, PaymentCode.CREDITO_AVISTA, paymentListener)
        }
        else if(tipoPgto == 4){
            orderManager!!.checkoutOrder(orderId!!, 550, PaymentCode.DEBITO_AVISTA, paymentListener)
        }
       // orderManager!!.checkoutOrder(orderId!!, 550, PaymentCode.CREDITO_PARCELADO_ADM, paymentListener)

    }

    fun impressao(){

        val printerManager = PrinterManager(this)

        //Callback impressao
        val printerListener = object: PrinterListener{
            override fun onPrintSuccess() {
                Log.d("CIELO", "onPrintSuccess")
            }

            override fun onError(e: Throwable?) {
                Log.d("CIELO", "onError")
            }

            override fun onWithoutPaper() {
                Log.d("CIELO","onWithoutPaper")
            }
        }

        //Mapa de estilos
        val alignLeft = HashMap<String, Int>()
        alignLeft[PrinterAttributes.KEY_ALIGN] = PrinterAttributes.VAL_ALIGN_LEFT
        alignLeft[PrinterAttributes.KEY_MARGIN_TOP] = 50
        alignLeft[PrinterAttributes.KEY_MARGIN_BOTTOM] = 50
        alignLeft[PrinterAttributes.KEY_TYPEFACE] = 0
        alignLeft[PrinterAttributes.KEY_TEXT_SIZE] = 20

        val textToPrint = "Texto simples a ser impresso.\n Com múltiplas linhas"
        printerManager.printText(textToPrint, alignLeft, printerListener)
    }
}
