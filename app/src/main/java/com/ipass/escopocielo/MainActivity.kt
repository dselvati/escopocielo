package com.ipass.escopocielo

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import kotlinx.android.synthetic.main.main_activity.*
import android.widget.ArrayAdapter



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        toolbar.title = "Cielo - Manager"
        setSupportActionBar(toolbar)

        var pedido = 0


        val listProdutos = arrayListOf("Coca-cola", "Cerveja")
        val adapter = ArrayAdapter<String>(this, R.layout.spinner_item, listProdutos)

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sp_produtos.adapter = adapter

        sp_produtos.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                tv_valor.text = when(position){
                                        0 ->  "R$ 5,00"
                                        else -> "R$ 8,00"
                                    }

                pedido = position
            }
        }

        btn_finalizar.setOnClickListener {
            Intent(this@MainActivity, KotlinActivity::class.java).apply {
                putExtra("pedido", pedido)
                startActivity(this)
            }
        }
    }
}